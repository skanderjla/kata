package com.adventium.kata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KataApplication {

    public static void main(String[] args) {
        OutputPrint outputs = new OutputPrint();
        SpringApplication.run(KataApplication.class, args);
        outputs.printBill();
    }
}
