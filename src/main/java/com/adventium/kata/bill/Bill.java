
package com.adventium.kata.bill;

import com.adventium.kata.entity.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class Bill {

    private static final Logger LOG = LoggerFactory.getLogger(Bill.class);

    static double totalTax;
    static double totalTtc;

    /**
     * @param orders List
     *               Affiche la facture détaillée des différents produits en param
     */
    public static void calculBill(List<Order> orders, String orderName) {
        totalTax = 0.0;
        totalTtc = 0.0;

        System.out.println(orderName);
        orders.forEach(x -> {
                    try {
                        System.out.println(x.getQuantity() + " " + x.getName() + " " + " " +
                                "à" + "  " + x.getPrice() + "€" + " " + ":" + " " + priceProductTtc(x) + "€");
                        totalTax += x.getTax() * x.getQuantity();
                        totalTtc += priceProductTtc(x);
                    } catch (Exception e) {
                        LOG.error("Calculation bill error" + e.getMessage());
                    }
                }
        );
        System.out.println("Montant des taxes :" + " " + totalTax);
        System.out.println("Total :" + " " + round2AfterDot(totalTtc) + "\n");
    }

    /**
     * @param order Calcul du prix ttc par produit.
     * @return
     */
    public static Double priceProductTtc(Order order) {
        double ttc = 0.0;
        order.setTax(productTaxe(order));
        try {
            ttc = (order.getPrice() + order.getTax()) * order.getQuantity();
        } catch (Exception e) {
            LOG.error("Prix TTC error" + e.getMessage());
        }
        return round2AfterDot(ttc);
    }

    /**
     * @param product Calcul des taxes par produit.
     * @return double
     */
    public static Double productTaxe(Order product) {

        double taxIport = 0.0;
        double taxe = 0.0;

        if (product.getPrice() != null && product.getPrice() != 0) {
            if (product.getImported()) {
                taxIport = product.getPrice() * 0.05;
            }

            if (product.getCategory() == ECategory.NOURRITURE || product.getCategory() == ECategory.MEDICAMENTS) {
                taxe = taxIport;
            } else if (product.getCategory() == ECategory.LIVRE) {
                taxe = product.getPrice() * 10 / 100 + taxIport;
            } else {
                taxe = product.getPrice() * 20 / 100 + taxIport;
            }
        }
        return roundPrice(Math.floor(taxe * 100) / 100);
    }

    /**
     * @param price prix arrondi aux 5 centimes supérieurs
     * @return double
     */
    public static Double roundPrice(Double price) {
        double val = 0;
        try {
            val = price * 100;
            if (val % 5 == 0) {
                return val / 100;
            } else
                return ((val - val % 5) + 5) / 100;
        } catch (Exception e) {
            LOG.error("prix arrondi error" + e.getMessage());
            return val;
        }
    }

    /**
     * @param nb retourne Double avec deux chiffres après la virgule
     * @return double
     */
    public static double round2AfterDot(double nb) {

        try {
            BigDecimal bd = new BigDecimal(Double.toString(nb));
            bd = bd.setScale(2, RoundingMode.HALF_UP);
            return bd.doubleValue();
        } catch (Exception e) {
            LOG.error("prix arrondi error" + e.getMessage());
            return nb;
        }
    }
}