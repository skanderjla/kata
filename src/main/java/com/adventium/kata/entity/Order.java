package com.adventium.kata.entity;

import com.adventium.kata.bill.ECategory;

public class Order {

    Long id;
    String name;
    ECategory category;
    Double price;
    Double tax;
    Boolean imported;
    Integer quantity;

    public Order(String name, ECategory category, Double price, Double tax, Boolean imported, Integer quantity) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.tax = tax;
        this.imported = imported;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ECategory getCategory() {
        return category;
    }

    public void setCategory(ECategory category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Boolean getImported() {
        return imported;
    }

    public void setImported(Boolean imported) {
        this.imported = imported;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}