package com.adventium.kata;

import com.adventium.kata.bill.Bill;
import com.adventium.kata.bill.ECategory;
import com.adventium.kata.entity.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OutputPrint {

    public void printBill() {
        Bill.calculBill(createOrder1(),"Facture 1");
        Bill.calculBill(createOrder2(),"Facture 2");
        Bill.calculBill(createOrder3(), "Facture 3");
    }


    public static List<Order> createOrder1() {
        List<Order> orders = new ArrayList<>();
        orders.add(new Order("livres", ECategory.LIVRE, 12.49, 0.0, false, 2));
        orders.add(new Order("CD musical", ECategory.AUTRES, 14.99, 0.0, false, 1));
        orders.add(new Order("barres de chocolat", ECategory.NOURRITURE, 0.85, null, false, 3));
        return orders;
    }

    public List<Order> createOrder2() {
        List<Order> orders = new ArrayList<>();

        orders.add(new Order("boîtes de chocolats importée", ECategory.NOURRITURE, 10.00, null, true, 2));
        orders.add(new Order("flacons de parfum importé", ECategory.AUTRES, 47.50, null, true, 3));
        return orders;
    }

    public List<Order> createOrder3() {
        List<Order> orders = new ArrayList<Order>();
        orders.add(new Order("flacons de parfum importé", ECategory.AUTRES, 27.99, null, true, 2));
        orders.add(new Order("flacon de parfum", ECategory.AUTRES, 18.99, null, false, 1));
        orders.add(new Order("boîtes de pilules contre la migraine", ECategory.MEDICAMENTS, 9.75, null, false, 3));
        orders.add(new Order("boîtes de chocolats importés", ECategory.NOURRITURE, 11.25, null, true, 2));
        return orders;
    }
}
