package com.adventium.kata;

import com.adventium.kata.bill.Bill;
import com.adventium.kata.bill.ECategory;
import com.adventium.kata.entity.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
public class BillTest {

    @Test
    public void productTaxeBook() {
        Order orders = new Order("livre", ECategory.LIVRE, 12.49, null, false, 1);
        Bill.productTaxe(orders);
        assertEquals(Bill.productTaxe(orders), 1.25);
    }

    @Test
    public void productTaxeBookImported() {
        Order orders = new Order("livre", ECategory.LIVRE, 12.49, null, true, 1);
        Bill.productTaxe(orders);
        assertEquals(Bill.productTaxe(orders), 1.9);
    }

    @Test
    public void productTaxeDrugs() {
        Order orders = new Order("migrainepil", ECategory.MEDICAMENTS, 9.75, null, false, 12);
        Bill.productTaxe(orders);
        assertEquals(Bill.productTaxe(orders), 0.0);
    }

    @Test
    public void productTaxeDrugsImported() {
        Order orders = new Order("migrainepil", ECategory.MEDICAMENTS, 9.75, null, true, 12);
        Bill.productTaxe(orders);
        assertEquals(Bill.productTaxe(orders), 0.5);
    }

    @Test
    public void productTaxeAliments() {
        Order orders = new Order("chocolat ", ECategory.NOURRITURE, 11.25, null, false, 12);
        Bill.productTaxe(orders);
        assertEquals(Bill.productTaxe(orders), 0.0);
    }

    @Test
    public void productTaxeAlimentsImported() {
        Order orders = new Order("chocolat", ECategory.NOURRITURE, 11.25, null, true, 12);
        Bill.productTaxe(orders);
        assertEquals(Bill.productTaxe(orders), 0.6);
    }


    @Test
    public void productTaxeOther() {
        Order orders = new Order("CD ", ECategory.AUTRES, 11.25, null, false, 12);
        assertEquals(Bill.productTaxe(orders), 2.25);
    }

    @Test
    public void productTaxeOtherImported() {
        Order orders = new Order("CD", ECategory.AUTRES, 11.25, null, true, 12);
        assertEquals(2.85, Bill.productTaxe(orders));
    }

    @Test
    public void roundPrice() {
        assertEquals(2.00, Bill.roundPrice(1.99));
        assertEquals(1.00, Bill.roundPrice(1.00));
        assertEquals(1.05, Bill.roundPrice(1.01));
        assertEquals(1.05, Bill.roundPrice(1.02));
    }

    @Test
    public void priceBookTTC() {
        Order order = new Order("livre", ECategory.LIVRE, 12.49, null, false, 2);
        assertEquals(27.48, Bill.priceProductTtc(order));
    }

    @Test
    public void priceAlimentTTC() {
        Order order = new Order("chocolat", ECategory.NOURRITURE, 0.85, null, false, 3);
        assertEquals(2.55, Bill.priceProductTtc(order));
    }

    @Test
    public void priceMedicamentTTC() {
        Order order = new Order("harissa", ECategory.MEDICAMENTS, 9.75, null, false, 3);
        assertEquals(29.25, Bill.priceProductTtc(order));
    }

    @Test
    public void priceOtherTTC() {
        Order order = new Order("CD", ECategory.AUTRES, 14.99, null, false, 1);
        assertEquals(17.99, Bill.priceProductTtc(order));
    }

    @Test
    public void round2AfterDot() {
        assertEquals(11.50, Bill.round2AfterDot(11.5000));
    }

}