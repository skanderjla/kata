# ADVENTIUM TECHNOLOGIES KATA APPLICATION

Minimal [Spring Boot](http://projects.spring.io/spring-boot/)

## Requirements

- [JDK 8](https://www.oracle.com/fr/java/technologies/javase/javase-jdk8-downloads.html)
- [Maven 3](https://maven.apache.org)

## Frameworks 

- Spring Boot 2.2.6
- spring-boot-starter-actuator 
- SonarQube 8 
- Junit 5

## Exécution l'application en local

Il existe plusieurs façons d'exécuter une application Spring Boot sur votre ordinateur local. Une façon consiste à exécuter la méthode `main` dans la classe kataApplication à partir de votre IDE.

Vous pouvez également utiliser le [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html):

```shell
mvn spring-boot:run
```

### NB :

- Selon l'énoncé de l'exercice il faut arrondir que la taxe 
mais dans l'output attendu, c'est le prix ttc qui est arrondi. Moi j'ai suivi l'énoncé et  j'ai arrondi que la taxe,
 c'est pour cela mes outputs ne correspondent pas aux outputs attendus dans l'énoncé.


###  Calculs :

- Produits de premières nécessités 0%
- Livre 10%
- Autres 20%
- Produits importés 5%

#### Output 1 :

Produit | HT | TAX | TTC |TOTAL| 
--- | --- | --- | --- |--- |
2 livres | 12.49 | 12.49*0.1 =1.24 => 1.25 |12.49+1.25 = 13.74 |13.74 * 2 = 27.48 |
1 CD | 14.99 | 14.99*0.2 =2.99 => 3 | 14.99+3 = 17.99 | 17.99 |
3 barres de chocolat | 0.85 | 14.99*0.0 =0 | 0.85+0 = 0.85 | 0.85 * 3 = 2.55 |

#### Output 2 :

Produit | HT | TAX | TTC |TOTAL| 
--- | --- | --- | --- |--- |
2 boîtes de chocolats importée | 10 | 10*0.05 =0.5 |10 + 0.5 = 10.5|10.5 *2 = 21|
3 flacons de parfum importé | 47.5 | 47.5*0.25 =11.87 => 11.9 | 47.5+11.9 = 59.4 | 59.4*3=178.2 |

#### Output 3 :

Produit | HT | TAX | TTC |TOTAL| 
--- | --- | --- | --- |--- |
2 flacons de parfum importé | 27.99 | 27.99*0.25=6.99 => 7 |27.99 + 7 = 34.99|34.99 *2 = 69.98|
1 flacon de parfum | 18.99 | 18.99*0.2 =3.79 => 3.8 | 18.99+3.8=22.79 | 22.79*1=22.79 |
3 boîtes de pilules contre la migraine | 9.75 | 9.75*0 =0 | 9.75+0=9.75 | 9.75*3=29.25 |
2 boîtes de chocolats importés | 11.25 | 11.25*0.05 =0.56 => 0.6 | 11.25+0.6=11.85 | 11.85*2=23.7 |

###  classes:

- TUs : BillTest
- Afficher la facture détaillée pour tous les paniers sur la console : KataApplication 




-------------------------------------------------------------
- [Jlaiel Skander](https://www.linkedin.com/in/skander-jlaiel/)
